section .data
 buf: times 256 db 0

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    xor rax, rax,
    mov rax, 60
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte[rdi+rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rsi, rdi
    call string_length
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret
   
; Принимает код символа и выводит его в stdout
print_char:
   xor rax, rax
   mov rax, 1
   push rdi
   mov rsi, rsp
   mov rdi, 1
   mov rdx, 1
   syscall
   pop rdi
   ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r8, rsp
    mov r9, 10
    mov rax, rdi
    dec rsp
    mov byte[rsp], 0

    .loop:
        dec rsp
        xor rdx, rdx
        div r9
        add rdx, 0x30
        mov byte[rsp], dl
        test rax, rax
        jnz .loop
    .print:
        mov rdi, rsp
        call print_string
        mov rsp, r8
        ret
    

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    mov rax, rdi ; перенесем аргумент в аккумулятор
    test rax, rax ; проставим флаги
    jns .plus ; если положительное число, то выводим через print_uint
    mov rdi, '-' ; иначе пишем минус
    push rax ; rax изменится, поэтому сохраним его
    call print_char
    pop rax ; восстановим
    neg rax ; приведем к положительному числу
    mov rdi, rax
    .plus:
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    .compare:
        mov r8b, byte[rdi+rcx]
        mov r9b, byte[rsi+rcx]
        cmp r8b, r9b
        jne .wrong
    .null_check:
        test r8b, r8b
        jz .success
        inc rcx
        jmp .compare
    .success:
        mov rax, 1
        jmp .exit
    .wrong:
        mov rax, 0
    .exit:
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec rsp
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    mov rsi, rsp
    syscall
    test rax, rax
    je .return
    mov rax, [rsp]

    .return:
    inc rsp
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rbx
    mov r9, rdi ; адрес начала буфера
    mov r8, rsi ; размер буфера
    xor rbx, rbx ; счетчик длины слова
    xor rdi, rdi ; 
    mov rdx, 1

    
    .spaces:
        xor rax, rax ; 0 stdin
        mov rsi, buf
        syscall
        cmp al, 0 ; если конец потока — завершаем чтение
        je .final
        cmp byte[buf], 0x20
        je .spaces
        cmp byte[buf], 0x9
        je .spaces
        cmp byte[buf], 0xA
        je .spaces
        inc rbx ; увеличиваем счетчик длины слова


    .read:
        xor rax, rax
        lea rsi, [buf+rbx]
        syscall
        cmp byte[buf+rbx], 0x21
        jb .final
        cmp r8, rbx 
        jbe .exit
        inc rbx 
        jmp .read


    .final: 
        mov byte[buf+rbx], 0
        mov rdx, rbx
        mov rax, buf
        pop rbx
        ret

    .exit:
        xor rdx, r8 ; колво символов
        xor rax, rax ; 0
        pop rbx
        ret

 ; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rsi, rsi
    mov r8, 10
    xor rcx, rcx 
 
 .read:
    mov sil, [rdi+rcx]
    cmp sil, '0'
    jb .exit
    cmp sil, '9'
    ja .exit
    inc rcx
    sub sil, '0'
    mul r8
    add rax, rsi
    jmp .read

 .exit:
    mov rdx, rcx
    ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    cmp byte[rdi], '-'
    je .sign
    call parse_uint
    jmp .exit

    .sign:
        inc rdi ; считаем знак
        call parse_uint  
        cmp rdx, 0
        je .exit
        neg rax
        inc rdx ; за знак

    .exit:
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rbx ; Этот регистр callee-saved, поэтому мы его кладем в стек, чтобы потом восстановить
    xor rax, rax ; Обнуляем аккумулятор, здесь будет храниться счетчик

    ;rdi указатель на строку
    ;rsi указатель на буфер
    ;rdx длина буфера

.loop:
    cmp rax, rdx ; Сравниваем счетчик с длиной буфера. Если они равны, и мы начали итерацию
    je .less     ; значит, все плохо — и строка не помещается.

    mov bl, byte [rdi+rax] ; Перемещаем в младший байт rbx (8 бит) восемь бит адреса текущего символа
    mov byte [rsi + rax], bl ; Перемещаем из младшего байта rbx (8 бит) текущий символ в  текущую ячейку буфера
    cmp byte [rsi + rax], 0  ; Если строка закончилась,
    je .exit                ; То выходим

    inc rax                  ; Если нет — то прибавляем к счетчику единицу
    jmp .loop                  ; И так по новой
.less:                            ;
    xor rax, rax                ; Обнуляем аккумулятор
.exit:
    pop rbx                     ; Восстанавливаем rbx
    ret                         ; И благополучно выходим